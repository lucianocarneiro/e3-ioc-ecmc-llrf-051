############################################################
############# NCL ASTEMP 

# 1  0:1   PREOP  +  EK1100-0010 EtherCAT-Koppler (2A E-Bus, FX-SingleMode, ID-Switc
# 2  0:2   PREOP  +  EL3202-0010 2K. Ana. Eingang PT100 (RTD), hochgenau
# 3  0:3   PREOP  +  EL3202-0010 2K. Ana. Eingang PT100 (RTD), hochgenau

ecmcEpicsEnvSetCalc("ASTEMP_NUM", "0")

# SLAVE 0
#Configure EL1100 EtherCAT Coupler
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "0")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=$(ECMC_SLAVE_NUM), HW_DESC=EK1100"

# SLAVE 1
## Configure EL3202-0010 PT100
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "1")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=1, HW_DESC=EL3202-0010"
#
## Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd

# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd

# SLAVE 2
## Configure EL3202-0010 PT100
ecmcEpicsEnvSetCalc("ECMC_SLAVE_NUM", "2")
${SCRIPTEXEC} ${ecmccfg_DIR}addSlave.cmd, "SLAVE_ID=2, HW_DESC=EL3202-0010"
#
## Configure channel 1 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8000")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd
# Configure channel 2 EL3202-0010 PT100
epicsEnvSet("ECMC_EC_SDO_INDEX",         "0x8010")
${SCRIPTEXEC} $(E3_CMD_TOP)/hw/ecmcEL3202-0010-Sensor-chX_NCL.cmd